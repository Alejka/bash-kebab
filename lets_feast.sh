#!/usr/bin/env bash
#set -x
####################################
#created by: Oleg
#purpose: start the oven or grill the meat on the frying pan .
#         lets eat !
#date: 0511/2020
#version: v1.0.2
####################################

. food_shopping.sh
. at_the_kitchen.sh
getting_the_table_ready=1
getting_drinks=1


getting_the_table_ready(){
  if [[ $getting_the_table_ready -eq 0 ]];then
     echo "Table is set"
 else
     echo "Putting plates , cutlery on the table"
     sleep 2
     echo "Table is set"
     sleep 2
     getting_the_table_ready=0
 fi
}
getting_drinks(){
  if [[ $getting_drinks -eq 0 ]];then
     echo "Drinkis are served"
 else
     echo "Putting the drinks on the table"
     sleep 2
     echo "Drinks are served"
     sleep 2
     getting_drinks=1
   fi
}

start_the_oven
#start_the_gas_for_grill
    sleep 2
    echo "Placing the Kebab inside the oven "
    sleep 2
    timer=200
    while [[ $timer -ne 0 ]]
    do
                echo ":Cooking the kebab:"
                    clear
                let timer--
    done
        echo "Kebab is Cooked"
    sleep 2
wrap_in_pita
    sleep 2
getting_the_table_ready
    sleep 2
    getting_drinks
    sleep 2
    echo "Plating the pitta wrapped kebab on the plates"
    sleep 2
figlet -c "Bon Apetite"
