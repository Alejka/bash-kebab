#!/usr/bin/env bash
#set -x
####################################
#created by: Oleg
#purpose: shopping at the market
#date: 05/11/2020
#version: v1.0.2
####################################

##### Variables -----------------------
meat_status=1
spices_status=1
pita_status=1
mix_status=1
cook_in_oven_status=1
cook_in_grill_status=1

get_meat(){
    if [[ $meat_status -eq 0 ]];then
        echo "Got Meat"
    else
        echo "Going to market to buy meat"
        sleep 2
        echo "Bought some lamb meat"
        meat_status=0
    fi

}
get_spices(){
     if [[ $spices_status -eq 0 ]];then
        echo "Got Spices"
    else
        echo "Enters the spices shop"
        sleep 2
        echo "Bought some paprika and white peper"
        spices_status=0
    fi
}
mix_it_all(){
    if [[ $mix_status -eq 0 ]];then
        echo "Already mixed"
            sleep 2
        echo "Let go on"
    else
       grind_meat_and_spices
    fi
}
cook_it_in_oven(){
    if [[ $cook_in_oven_status -eq 0 ]];then
        echo "Kebab is Done"
        sleep 2
    else
        echo "Turning ON the oven"
        sleep 2
        echo "Putting the kebab inside the oven"
        sleep 2
        timer=100
    while [[ $timer -ne 0 ]]
    do
                echo ":Cooking:"
                    clear
                let timer--
    done
        echo "Kebab is done"
        sleep 2
        clear
        cook_in_oven_status=0
    fi
}

cook_it_on_grill(){

      if [[ $cook_in_grill_status -eq 0 ]];then
          echo "Kebab is Done"
          sleep 2
      else
          echo "Turning ON the grill"
          sleep 2
          echo "Putting the kebab inside the grill"
          sleep 2
          timer=100
      while [[ $timer -ne 0 ]]
      do
                  echo ":Cooking:"
                      clear
                  let timer--
      done
          echo "Kebab is done"
          sleep 2
          clear
          cook_in_grill_status=0
        fi
}
grind_meat_and_spices(){
     echo "Let mix it up: "
     sleep 2
        timer=200
        while [[ $timer -ne 0 ]]
            do
                echo ":Mixing:"
                    clear
                let timer--
          done
        echo "Finished mixing"
      #done
        sleep 2
        clear

        mix_status=0
}
wrap_in_pita(){
  if [[ $pita_status -eq 0 ]];then
     echo "Already wrapped"
 else
     echo "Getting ready to wrap in pita"
     sleep 2
     timer=200
 while [[ $timer -ne 0 ]]
 do
             echo ":Wrapping:"
                 clear
             let timer--
 done
     echo "Kebab is wrapped in pita"
     sleep 2
     clear
     pita_status=0
 fi
}
