#!/usr/bin/env bash
#set -x
####################################
#created by: Oleg
#purpose: start the oven or grill the meat on the frying pan
#date: 05/11/2020
#version: v1.0.2
####################################

. food_shopping.sh
start_the_oven=1
start_the_gas_for_grill=1
get_meat
    sleep 2
#    echo "Now i need spices !"
#    sleep 2
get_spices
    sleep 2
     echo "Heading home now ."
     sleep 2
     echo "Now i grind the meat and the spices."
     sleep 2
     grind_meat_and_spices
     sleep 2

start_the_oven(){
  if [[ $start_the_oven -eq 0 ]];then
      echo "Oven is hot"
      sleep 2
  else
      echo "Turning ON the oven"
      sleep 2
      echo "Swithcing to 200 degrees celsius."
      sleep 2
      timer=200
  while [[ $timer -ne 0 ]]
  do
              echo ":Heating up:"
                  clear
              let timer--
  done
      echo "Oven is ready"
      sleep 2
      clear
      start_the_oven=0
  fi
}
start_the_gas_for_grill(){
    if [[ $start_the_gas_for_grill -eq 0 ]];then
        echo "Gas is already on"
        sleep 2
    else
        echo "turning ON the gas"
        sleep 2
        echo "Gas is on flames are out."
        sleep 2
        start_the_gas_for_grill=0
    fi
}
